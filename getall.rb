require 'mechanize'
require 'logger'
require 'fileutils'

DESTINATION = "/Users/crandquist/Pictures/dailymugshot.com/"
VERBOSE = true

def get_user number
  user_start = Time.now

  agent = Mechanize.new { |a| a.log = Logger.new("dms.log") }

  agent.user_agent_alias = "Mac Safari"

  puts "\nGetting page for #{number}"

  begin
    page = agent.get("http://www.dailymugshot.com/authusers/#{number}")
  rescue
    return
  end

  owner = page.search("h1")

  if owner.count < 2
    puts "headers not found"
    return
  end
  owner = owner[1].inner_text.gsub("Mugshow","").gsub("'s","").strip

  images = page.search("div#mosaic div.thumb img")

  dir_path = "#{DESTINATION}#{number}-#{owner}/"

  if images.count < 10
    puts "User has less than 10 images skipping"

    if File.exists?(dir_path)
      puts "removing previously created directory"
      FileUtils.rm_rf(dir_path)
    end
    return
  end

  unless File.exists?(dir_path)
    puts "Creating directory '#{dir_path}'"

    FileUtils.mkdir_p(dir_path)

    puts "created directory #{dir_path}"
  else
    puts "Directory exists"
    if File.exists?("#{dir_path}working")
      puts "in process by another task"
      return
    else
      unless (Dir.open(dir_path).collect.count -2) == images.count
        # fall on through and process
      else
        puts "all images already exist"
        return
      end
    end
  end

  puts "\tFetching #{images.count} images"

  FileUtils.touch("#{dir_path}working")

  skip = 0

  images.each_with_index do |image_tag, i|
    img_src_url = image_tag.attribute("src").to_s.gsub("thumb","full")

    temp_name = img_src_url.split("/").last.gsub("?","_").strip

    file_name = "#{dir_path}#{'%03d' % i}-dms.jpg"
    unless File.exists?(file_name)
      puts "Saving image at #{img_src_url} to #{temp_name}" if VERBOSE

      begin
        image = agent.get(img_src_url)
      rescue
        print "X"
        next
      end

      image.save

      puts "moving #{temp_name} to #{file_name}" if VERBOSE

      FileUtils.mv("/Users/crandquist/projects/personal/dailymugshot/#{temp_name}", file_name)
      if i % 25 == 0 && i != 0
        puts "\t#{images.count - i} to go - elapsed time #{Time.now-user_start}"
      end
      print "."
    else
      if i % 25 == 0  && i != 0
        puts "\t#{images.count - i} to go - elapsed time #{Time.now-user_start}"
      end
      print ">"
      skip +=1
    end

  end
  puts "\n"
  puts "\tSkipped #{skip} image(s)"
  puts "\tProcessed user in #{Time.now-user_start}"
  FileUtils.rm("#{dir_path}working")
end

# Program execution

start_at = 0

puts "Enter your user number:"
user_number = gets.chomp

if user_number.empty?
  user_number = "27857"
elsif user_number =~ /\+/
  start_at = user_number.to_i
  user_number = "all"
end

puts "*#{user_number}*"

if user_number == "all"
  (start_at..99999).each do |user_number|
    get_user user_number
  end
else
  get_user user_number
end
